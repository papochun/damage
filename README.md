# Damage Calculator

This program was written to calculate the average damage dealt per second using primary and secondaries


## Installation

Head over to: [The Go Programming Language](https://go.dev).  
Select **Download**, and select whatever platform you intend to install on.  
Once installed, you can run the command below in your terminal.  

```
go install gitlab.com/papochun/damage@latest
```


## Contact

You can contact me via this email:  
<papochun.unnamed123@slmail.me>
