package main

import (
	"fmt"
	"strconv"
	"log"
)

/*
Converts text input string into float64.
Divison added to convert percentage to decimal.
Offset added to turn percentage into multipliers.
*/
func getValue(prompt string, division float64, offset float64) (float64) {
	fmt.Printf(prompt)
	var buffer string
	fmt.Scanln(&buffer)

	buffer_float, err := strconv.ParseFloat(buffer, 64)
	if err != nil {
		log.Fatal(err)
	}
	return buffer_float / division + offset
}

/*
Calculates the average amount of damage with the potential to crit.
*/
func calculateCrit(base float64, cc float64, cm float64) (float64) {
	var total float64
	total = cm - 1
	total = total * cc + 1
	return base * total
}

func galAptitude(total float64) (float64) {
	var buffer string

	fmt.Printf("Are you using Galvanized Aptitude [Y/n]: ")
	fmt.Scanln(&buffer)

	if buffer == "n" {
		return total
	}
	modded_damage := getValue("Modded Damage Percent: ", 100, 1)
	modded_total := modded_damage + float64(0.80)
	/* remove damage mods */
	total = total / modded_damage
	/* reapply damage mods with galvanized bonus */
	return total * modded_total
}

func galChamber(total float64, modded_mult float64) (float64) {
	var buffer string

	fmt.Printf("Are you using Galvanized Chamber [Y/n]: ")
	fmt.Scanln(&buffer)

	if buffer == "n" {
		return total
	}

	base_mult := getValue("Unmodded Multishot: ", 1, 0)

	/* remove multishot from total damage */
	total = total / modded_mult
	/* get modded multishot without galvanized bonus */
	diff_mult := modded_mult / base_mult
	/* add galvanized bonus */
	total_mult := diff_mult + 1.5
	/* reapply multishot with galvanized bonus */
	return total * total_mult
}

func main() {
	total := getValue("Total Damage: ", 1, 0)
        fact  := getValue("Faction Damage: ", 100, 1)
        cc    := getValue("Critical Chance: ", 100, 0)
        cm    := getValue("Critical Multiplier: ", 1, 0)
        fr    := getValue("Fire Rate: ", 1, 0)
	mult  := getValue("Multishot: ", 1, 0)

	fmt.Print("Conditional mods require extra information to calculate\n")
	total = galAptitude(total)
	total = galChamber(total, mult)

	/* average damage with crit in mind */
	hit := calculateCrit(total, cc, cm)
	/* add potential faction damage when applicable*/
	hit = hit * fact
	/* calculates the damage per second */
	dps := hit * fr

	/* print */
	fmt.Printf("Hit: %0.f\n", hit)
	fmt.Printf("DPS: %0.f\n", dps)
}
